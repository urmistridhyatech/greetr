<?php


use Simplexi\Greetr\Greetr;


use Illuminate\Support\ServiceProvider;

class GreetServiceProvider extends ServiceProvider
{
    /**
     * Run boot operations.
     */
    public function boot()
    {
        $config = __DIR__.'/Config/config.php';

        $this->publishes([
            $config => config_path('Greetr.php'),
        ]);

        $this->mergeConfigFrom($config, 'greetr');
    }

    /**
     * Register the location binding.
     */
    public function register()
    {
        $this->app->bind('greetr', Greetr::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['greetr'];
    }
}
